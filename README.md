# CHEETAH - Server #

### Requirements ###
* Node v14+
* Npm v6+

### How do I get set up? ###
* Clone repo
* run ```npm install```

### Start To Development ###
* Run ```npm start``` for development server
* Server work on ```http://localhost:3000``` 
* MongoDB work on Cloud 
