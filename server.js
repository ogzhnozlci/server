const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');


const dbConfig = require('./config/db.config.js');
const port = process.env.PORT || 3000;

const app = express();
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())


const connectionParams={
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
}
mongoose.connect(dbConfig.url,connectionParams)
    .then( () => {
        console.log('Connected to database ')
    })
    .catch( (err) => {
        console.error(`Error connecting to the database. \n${err}`);
    })

const responseMW = require('./app/middlewares/response')
app.use(responseMW);

// define a simple route
app.get('/', (req, res) => {
    res.json({"message": "Welcome to EasyNotes application. Take notes quickly. Organize and keep track of all your notes."});
});


const authMW = require('./app/middlewares/auth')
app.use(/^\/(?!user).*/, authMW);


require("./app/routes/category.routes")(app);
require("./app/routes/user.routes")(app);
require("./app/routes/task.routes")(app);

// listen for requests
app.listen(port, () => {
    console.log("Server is listening on port 3000");
});
