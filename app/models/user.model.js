const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    isTasker: Boolean,
    createdAt: {
        type: Date,
        default: Date.now()
    }
}) ;

module.exports = mongoose.model("User",UserSchema);
