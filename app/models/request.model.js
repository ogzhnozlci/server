const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RequestSchema = new mongoose.Schema({
    task: {
        type: Schema.Types.ObjectId,
        ref: 'Task'
    },
    value: {
        type: Number,
        required: true
    },
    currency: {
        type: String,
        default: "USD"
    },
    request_creator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    createdAt: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model("Request",RequestSchema);
