const mongoose = require('mongoose');

const CategoryModel = mongoose.Schema({
    title: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('Category', CategoryModel);
