module.exports =  function (req, res, next) {
    res.success = (data) => {
        let resp = {
            success:true,
            data
        };

        if (Array.isArray(data)) {
            resp['length'] = data.length;
        }

        res.send(resp)
    }
    next();
}
