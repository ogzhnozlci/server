const jwt = require("jsonwebtoken");
const CONSTANTS = require("../constants/constants");

module.exports = function(req, res, next) {
    const token = req.header(CONSTANTS.auth.auth_header_key);
    if (!token) return res.status(401).json({ message: "Auth Error" });
    try {
        const decoded = jwt.verify(token, CONSTANTS.auth.secret_key_auth);
        req.user = decoded.user;
        next();
    } catch (e) {
        console.error(e);
        res.status(500).send({ message: "Invalid Token" });
    }
};
