
module.exports = (app) => {
    const tasks = require('../controllers/task.controller.js');

    app.post('/task', tasks.create);

    app.get('/task', tasks.findAll);

    app.get('/tasks/category/:categoryId', tasks.findTaskAtCategory);

    app.get('/tasks/own', tasks.findUserOwnTasks);

    app.get('/task/requests/:taskId', tasks.findRequestOnTask);

    app.post('/task/request/:taskId', tasks.findTaskAndGiveRequest);

    app.get('/tasks/requests/own', tasks.findTasksGivenRequest);

}
