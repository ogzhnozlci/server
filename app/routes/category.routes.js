
module.exports = (app) => {
    const categories = require('../controllers/category.controller.js');

    app.post('/category', categories.create);

    app.get('/category', categories.findAll);
}
