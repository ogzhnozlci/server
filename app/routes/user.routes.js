const { body } = require('express-validator');

module.exports = (app) => {
    const users = require('../controllers/user.controller.js');

    app.post('/user/register',
        body('username', "Please Enter a Valid Username").not().isEmpty(),
        body('email', "Please enter a valid email").isEmail(),
        body('password', "Please enter a valid password").isLength({ min: 6 }),
        users.create);

    app.post('/user/login',
        body('email', "Please enter a valid email").isEmail(),
        body('password', "Please enter a valid password").isLength({ min: 6 }),
        users.findOne);

    //app.get('/user/:userId', users.findOne);

    //app.put('/user/:userId', users.update);

    //app.delete('/user/:userId', users.delete);
}
