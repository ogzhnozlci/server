module.exports = {
    auth : {
        secret_key_auth: "cheetah_auth",
        secret_key_refresh: "cheetah_refresh",
        auth_token_exp: "10h",
        refresh_token_exp: "7d",
        auth_header_key: "authorization"
    }
};
