const Task = require('../models/task.model');
const Request = require('../models/request.model');

exports.create = (req, res) => {
    console.log(req.user);

    const task = new Task({
        title: req.body.title,
        content: req.body.content,
        category: req.body.category,
        creator: req.user.id
    });

    task.save()
        .then(data => {
            res.success(data);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

exports.findAll = (req, res) => {
    Task.find()
        .populate('creator', '_id username email')
        .populate('category')
        .then(data => {
            res.success(data);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.findTaskAtCategory = (req, res) => {
    Task.find({
        category: req.params.categoryId
    })
        .populate('creator', '_id username email')
        .populate('category')
        .then(data => {
            res.success(data);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.findUserOwnTasks = (req, res) => {
    Task.find({
        creator: req.user.id
    })
        .populate('category')
        .then(data => {
            res.success(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving notes."
            });
        });
};


exports.findRequestOnTask = (req, res) => {
    Request.find({
        task: req.params.taskId
    })
    .populate({
        path: 'task',
        model: 'Task',
        populate: {
            path: 'creator',
            model: 'User',
            select: "_id username email"
        }
    })
    .populate({
        path: 'request_creator',
        model: 'User',
        select: "_id username email"
    })
        .populate('category')
        .then(task => {
            res.success(task);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving notes."
            });
        });
};

exports.findTaskAndGiveRequest = (req, res) => {
    const request = new Request({
        task: req.params.taskId,
        value: req.body.value,
        request_creator: req.user.id
    });

    request.save()
        .then(data => {
            res.success(data);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

exports.findTasksGivenRequest = (req, res) => {
    Request.find({
        request_creator: req.user.id
    })
        .populate({
        path: 'task',
        model: 'Task',
        populate: {
            path: 'creator',
            model: 'User',
            select: "_id username email"
        }
    })
        .populate({
            path: 'request_creator',
            model: 'User',
            select: "_id username email"
        })
        .populate('category')
        .then(data => {
            res.success(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving notes."
            });
        });
};
