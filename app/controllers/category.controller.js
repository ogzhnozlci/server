const Category = require('../models/category.model');

exports.create = (req, res) => {
    const category = new Category({
        title: req.body.title || "Untitled Note"
    });
    category.save()
        .then(data => {
            res.success(data);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

exports.findAll = (req, res) => {
    Category.find()
        .then(categories => {
            res.success(categories);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.findOne = (req, res) => {
    Category.findById(req.params.categoryId)
        .then(category => {
            if(!category) {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.noteId
                });
            }
            res.success(category);
        }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.noteId
        });
    });
};

exports.update = (req, res) => {
    if(!req.body.content) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    Category.findByIdAndUpdate(req.params.categoryId, {
        title: req.body.title || "Untitled Note",
    }, {new: true})
        .then(category => {
            if(!category) {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.categoryId
                });
            }
            res.success(category);
        }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.categoryId
            });
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.categoryId
        });
    });
};

exports.delete = (req, res) => {
    Category.findByIdAndRemove(req.params.categoryId)
        .then(category => {
            if(!category) {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.categoryId
                });
            }
            res.success({message: "Note deleted successfully!"});
        }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.categoryId
            });
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.categoryId
        });
    });
};
