const User = require('../models/user.model');
const { validationResult } = require('express-validator');
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const CONSTANTS = require("../constants/constants")

exports.create = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }

    const {
        username,
        email,
        password,
        isTasker
    } = req.body;

    try {
        let user = await User.findOne({
            email
        });
        if (user) {
            return res.status(400).json({
                msg: "User Already Exists"
            });
        }

        user = new User({
            username,
            email,
            password,
            isTasker
        });

        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password, salt);

        await user.save();

        const payload = {
            user: {
                id: user._id,
                type: isTasker ? "tasker" : "customer"
            }
        };

        jwt.sign(
            payload,
            CONSTANTS.auth.secret_key_auth, {
                keyid: user._id.toString(),
                expiresIn: CONSTANTS.auth.auth_token_exp
            },
            (err, token) => {
                if (err) throw err;
                res.success({
                    token,
                    username,
                    email,
                    isTasker,
                })
            }
        );
    } catch (err) {
        res.status(500).send("Error in Saving");
    }

};

exports.findOne = async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }

    const { email, password } = req.body;

    try {
        let user = await User.findOne({
            email
        });

        if (!user)
            return res.status(400).json({
                message: "User Not Exist"
            });

        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch)
            return res.status(400).json({
                message: "Incorrect Password !"
            });

        const payload = {
            user: {
                id: user._id,
                type: user.isTasker ? "tasker" : "customer"
            }
        };

        jwt.sign(
            payload,
            CONSTANTS.auth.secret_key_auth,
            {
                keyid: user._id.toString(),
                expiresIn: CONSTANTS.auth.auth_token_exp
            },
            (err, token) => {
                if (err) throw err;
                res.success({
                    token,
                    username : user.username,
                    email : user.email,
                    isTasker: user.isTasker
                })
            }
        );
    } catch (e) {
        console.error(e);
        res.status(500).json({
            message: "Server Error"
        });
    }
};
